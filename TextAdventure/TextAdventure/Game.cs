﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace TextAdventure
{
    public class Game
    {
        private String gameName = " ";   // a String initialized to empty
        private bool shouldExit = false; // Create a bool (true/false logical operator) indicating if we should exit the game

        // Constructor for the Game class
        // This is the code that is called whenever an instance of Game is created
        public Game()
        {

        }

        // Function (segment of logic) called from Program.cs when the application starts
        public void Setup()
        {
            // Storing the String "My First Game" (without quotes) in the variable named "gameName"
            gameName = "A Pretty Cool Game";
        }

        public void Begin()
        {
            // Print various lines of strings to the Console window
            Console.WriteLine("************************************");
            Console.WriteLine("Welcome to " + gameName);    // the + operator concatenates two strings (combines them)
            Console.WriteLine("You can do stuff, things, or more stuff.");

            // Loop until shouldExit is true
            while (shouldExit == false)
            {
                Console.Write("What stuff are you going to do?   ");  // Write a partial line
                String command = Console.ReadLine();    // Read a line from Console (when Enter is hit) and store it in command

                HandleCommand(command);
            }
        }


        public void HandleCommand(String command)
        {
            // Enforce that the String is lowercase
            command = command.ToLower();

            if (command == "stuff")
            {
                Console.WriteLine("************************************");
                Console.WriteLine("Somehow, you end up in a dark alley.");
                Console.WriteLine("An sketchy guy walks up to you, he looks like he's picking a fight. You can run, or you can fight.");

                String stuffcommand = Console.ReadLine();

                if (stuffcommand == "fight")
                {
                    Console.WriteLine("************************************");
                    Console.WriteLine("Foolishly, you stay and fight. He makes short work of you, and takes all of your money and dear possessions.");
                    for (int i = 0; i < 5; i++)
                    {
                        Console.WriteLine("YOU LOSE");
                    }
                }

                else if (stuffcommand == "run")
                {
                    Console.WriteLine("************************************");
                    Console.WriteLine("You run away from the man, and you safely escape.");
                    Console.WriteLine("You find yourself at an abandoned warehouse. You enter the building to find it filled with zombies. However, you spy a pile of gold at the other side of the room.");
                    Console.WriteLine("However, you also spy a door to a dark basement at the side of the room.");
                    Console.WriteLine("You can pick either gold or the door.");

                    String zombiecommand = Console.ReadLine();

                    if (zombiecommand == "gold")
                    {
                        Console.WriteLine("************************************");
                        Console.WriteLine("You bravely fight your way through the crowd of zombies with your bare hands, and make your way to the gold.");
                        Console.WriteLine("Triumphantly, you walk home with your pockets full of gold.");
                        Console.WriteLine("Congratulations, you won a pretty cool game. Don't type in anything else, because you won and there is nothing left.");
                    }

                    else if (zombiecommand == "door")
                    {
                        Console.WriteLine("************************************");
                        Console.WriteLine("You foolishly enter the suspiscious-looking door. You are lost in the darkness forever.");
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine("YOU LOSE");
                        }
                    }
                  
                }  
         
            }
            else if (command == "things")
            {
                Console.WriteLine("************************************");
                Console.WriteLine("After doing some things, you end up in a dark cave. On your left is a glowing magic sword, and on your right is a glowing magical staff. Choose one as your weapon.");

                String weaponcommand = Console.ReadLine();

                if (weaponcommand == "sword")
                {
                    Console.WriteLine("************************************");
                    Console.WriteLine("Immediately after you grasp the sword, a knight in armor approaches you from behind. From the looks of it, he was looking for the sword too.");
                    Console.WriteLine("He draws his sword. There's no running from this fight, you must defeat him!");
                    Console.WriteLine("The samurai lunges. You can either dodge or parry his attack. Choose wisely.");

                    String fightcommand = Console.ReadLine();

                    if (fightcommand == "dodge")
                    {
                        Console.WriteLine("************************************");
                        Console.WriteLine("You attempt to dodge, but sadly, you are fat and slow. The samurai strikes you down and takes the sword.");
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine("YOU LOSE");
                        }
                    }
                    else if (fightcommand == "parry")
                    {
                        Console.WriteLine("************************************");
                        Console.WriteLine("You quickly bring your sword to your face, blocking the knight's attack. You then finish off the stunned knight.");
                        Console.WriteLine("You see a glowing stone fall out of the armor.");
                        Console.WriteLine("You can choose to trash the stone or to keep it in your pocket.");

                        String stonecommand = Console.ReadLine();
                        
                        if (stonecommand == "keep")
                        {
                            Console.WriteLine("************************************");
                            Console.WriteLine("As you slip the stone in your pocket, the stone suddenly explodes, killing you and ending your journey.");
                            for (int i = 0; i < 5; i++)
                            {
                            Console.WriteLine("YOU LOSE");
                            }
                        }
                        else if (stonecommand == "trash")
                        {
                            Console.WriteLine("************************************");
                            Console.WriteLine("You toss the stone, but as it hits the floor, the stone turns into a pile of gold, all yours for the taking.");
                            Console.WriteLine("Congratulations, you won a pretty cool game. Don't type in anything else, because you won and there is nothing left.");
                        }
                    }
                }

                else if (weaponcommand == "staff")
                {
                    Console.WriteLine("************************************");
                    Console.WriteLine("As you collect the staff, a wizard approaches you from behind. From the looks of it, he was looking for the staff too.");
                    Console.WriteLine("He prepares to attack. There's no running from this fight, you must defeat him!");
                    Console.WriteLine("The wizard shoots a magic spell at you. You can either dodge the attack or reflect it right back at him.");

                    String magiccommand = Console.ReadLine();

                    if (magiccommand == "dodge")
                    {
                        Console.WriteLine("************************************");
                        Console.WriteLine("You successfully dodge the spell, and shoot one right back at the wizard. The wizard explodes, and you see a glowing stone appear where the wizard once stood.");
                        Console.WriteLine("You can choose to trash the stone or to keep it in your pocket.");

                        String stone2command = Console.ReadLine();

                        if (stone2command == "trash")
                        {
                            Console.WriteLine("************************************");
                            Console.WriteLine("You trash the stone, but you find that you have nothing left to live for, and you die of boredom.");
                            for (int i = 0; i < 5; i++)
                            {
                                Console.WriteLine("YOU LOSE");
                            }
                        }

                        else if (stone2command == "keep")
                        {
                            Console.WriteLine("************************************");
                            Console.WriteLine("You slip the stone into your pocket, where you find that it immediately turns into gold.");
                            Console.WriteLine("Congratulations, you won a pretty cool game. Don't type in anything else, because you won and there is nothing left.");
                        }
                    }

                    else if (magiccommand == "reflect")
                    {
                        Console.WriteLine("************************************");
                        Console.WriteLine("Since you can't actually do magic, you are hit by the magic spell and you explode.");
                        for (int i = 0; i < 5; i++)
                            {
                                Console.WriteLine("YOU LOSE");
                            }
                    }
                }
            }

            else if (command == "more stuff")
            {
                Console.WriteLine("************************************");
                Console.WriteLine("After doing more stuff, you find yourself in a large maze.");
                Console.WriteLine("The maze is huge, but you can either escape it or get lost in it.");

                String mazecommand = Console.ReadLine();

                if (mazecommand == "escape")
                {
                    Console.WriteLine("************************************");
                    Console.WriteLine("You escape the maze, and you find a pile of gold, but you realize that you have nothing else to do, and you are bored.");
                    for (int i = 0; i < 5; i++)
                    {
                        Console.WriteLine("YOU LOSE");
                    }
                }
                
                else if (mazecommand == "get lost")
                {
                    Console.WriteLine("************************************");
                    Console.WriteLine("Even though you get lost in the maze, you find yourself and come understand who you are as a person.");
                    Console.WriteLine("Congratulations,  you have completed a pretty cool game with the best possible ending. Now get off the computer and go outside.");
                }
            }

            else
            {
                Console.WriteLine("I don't know what you just said.");
            }
            
}
    }
}